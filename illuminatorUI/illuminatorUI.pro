#-------------------------------------------------
#
# Project created by QtCreator 2015-04-28T11:13:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = illuminatorUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    error_dialog.cpp \
    unitTestingFile.cpp \
    loadprofiles.cpp \
    Profile.cpp \
    AdditionalFunctions.cpp \
    passwordprompt.cpp \
    createprofile.cpp \
    colorpicker.cpp \
    loadselector.cpp \
    fileprompt.cpp \
    exehistorybrowser.cpp \
    loadmonitor.cpp \
    colordisplay.cpp \
    loadMonitorExecution.cpp \
    cpumem.cpp

HEADERS  += mainwindow.h \
    error_dialog.h \
    unitTestingFile.h \
    loadprofiles.h \
    ProfileDeclaration.h \
    AdditionalFunctions.h \
    passwordprompt.h \
    createprofile.h \
    colorpicker.h \
    loadselector.h \
    fileprompt.h \
    exehistorybrowser.h \
    loadmonitor.h \
    colordisplay.h \
    loadMonitorExecution.h \
    CPUMem.h

FORMS    += mainwindow.ui \
    error_dialog.ui \
    loadprofiles.ui \
    passwordprompt.ui \
    createprofile.ui \
    colorpicker.ui \
    loadselector.ui \
    fileprompt.ui \
    exehistorybrowser.ui \
    loadmonitor.ui \
    colordisplay.ui
