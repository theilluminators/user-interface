#include "ProfileDeclaration.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "error_dialog.h"
#include "unitTestingFile.h"
#include "loadprofiles.h"
#include "AdditionalFunctions.h"
#include "createprofile.h"
#include "loadselector.h"
#include <vector>
#include <string>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loadButton_clicked()
{
    std::vector<std::string> profileNameList = GetProfiles();
    error_dialog *noProfiles = new error_dialog;
    loadProfiles *profileListDialog = new loadProfiles;
    createProfile *makeProfileUI = new createProfile;
    loadSelector *loadSelectionUI = new loadSelector;
    //Show an error if there are no available profiles and start the create profile ui
    if (profileNameList.size() == 0)
    {
        noProfiles->setMessage("No Profiles!");
        noProfiles->exec();
        makeProfileUI->exec();
        loadSelectionUI->currentProfile=makeProfileUI->newProfile;
        this->close();
        loadSelectionUI->exec();
    }
    //If there are available profiles, show a list from which to choose one
    else {
        //Pass the available profiles to the ui
        profileListDialog->setValues(profileNameList);
        //Show the ui, only returns on valid profile selection or if cancel is clicked
        profileListDialog->exec();
        //If the dialog was not canceled...
        if(!profileListDialog->canceled)
        {
            //Retrieve the selected profile
            loadSelectionUI->currentProfile=profileListDialog->getChosenProfile();
            this->close(); //close the initial ui and move on to the load monitor
            loadSelectionUI->exec();
        }
    }
}

void MainWindow::on_createButton_clicked()
{
    loadSelector *loadSelectionUI = new loadSelector;
    createProfile *makeProfileUI = new createProfile();
    makeProfileUI->exec();
    loadSelectionUI->currentProfile=makeProfileUI->newProfile;
    this->close();
    loadSelectionUI->exec();
}

void MainWindow::on_skipButton_clicked()
{
    loadSelector *loadSelectionUI = new loadSelector;
    Profile currentProfile;
    loadSelectionUI->currentProfile=currentProfile;
    this->close();
    loadSelectionUI->exec();
}
