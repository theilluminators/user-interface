#include "exehistorybrowser.h"
#include "ui_exehistorybrowser.h"
#include "error_dialog.h"

exeHistoryBrowser::exeHistoryBrowser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::exeHistoryBrowser)
{
    ui->setupUi(this);
}

exeHistoryBrowser::~exeHistoryBrowser()
{
    delete ui;
}

void exeHistoryBrowser::on_cancelButton_clicked()
{
    filePath="";
    this->close();
}

void exeHistoryBrowser::on_chooseButton_clicked()
{
    filePath = ui->pathList->currentItem()->text().toStdString();
}

void exeHistoryBrowser::setFilePaths(std::vector<std::string> inputPaths)
{
    for(unsigned int i=0; i<inputPaths.size(); i++)
    {
        ui->pathList->addItem(QString::fromStdString(inputPaths.at(i)));
    }
}
