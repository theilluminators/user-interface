/*
 *
 * The start window, which presents the initial selection for choosing a profile type.
 * It is only closed after selecting a type of profile.
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_loadButton_clicked();

    void on_createButton_clicked();

    void on_skipButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
