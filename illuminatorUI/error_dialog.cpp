#include "error_dialog.h"
#include "ui_error_dialog.h"

error_dialog::error_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::error_dialog)
{
    ui->setupUi(this);
    ui->err_text->adjustSize();
}

error_dialog::~error_dialog()
{
    delete ui;
}

//Closes immediately one the OK button is clicked.
void error_dialog::on_err_button_clicked()
{
    this->close();
}

//Set the message for the error dialog to occur
//No input sanitation is done, careful what you enter!
void error_dialog::setMessage(std::string error) {
    ui->err_text->setText(QString::fromStdString(error));
}
