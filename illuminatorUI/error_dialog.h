/*
 *
 * This form  will be used throughout the project as an alert display box. While mostly used for errors,
 * having a message that can be set to any input string means this form can be used for any alert that
 * requires the user to simply hit "OK".
 *
 * For example, it is used as the "Password Accepted" display.
 *
 * Rather than creating a new element and setting the message, consider creating a single one and changing
 * the message before showing it to save on memory allocation. Passing that dialog would just be a pain
 * though, so don't bother with that unless you really really need to!
 *
 * The default error message is an unhelpful "Error!", only left there in case the input is not correctly set.
 * No input sanitation on the message occurs, make sure you don't display anything you didn't want to,
 * like a blank message.
 *
 */

#ifndef ERROR_DIALOG_H
#define ERROR_DIALOG_H

#include <QDialog>

namespace Ui {
class error_dialog;
}

class error_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit error_dialog(QWidget *parent = 0);
    ~error_dialog();
    void setMessage(std::string);

private slots:
    void on_err_button_clicked();

private:
    Ui::error_dialog *ui;
};

#endif // ERROR_NOPROFILES_H
