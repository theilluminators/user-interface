/*
CPU and Mem usage header file
Some original code by: Jeremy Friesner (each function he made is marked)
https://stackoverflow.com/questions/23143693/retrieving-cpu-load-percent-total-in-windows-with-c

Coded by: Ryan Dean
Changelog:
4/22/2015 - Tested original code for functionality
4/23/2015 - Changed getCPU function to comply with detail design
4/29/2015 - Wrote even more comments, made memory usage function, made mem and cpu records function, 
			made setColor function.

This code is responsible for all cpu usage and memory usage functions.
GetSystemTimes and GETMEMORYSTATUSEX are both microsoft windows api functions, which is 
where I get the cpu and memory usage from.

While some of the CPU code is from Jeremy Friesner I made 100% sure that I completely understand
how it works by doing extensive research.  Hopefully my explainations are clear.
*/

#include"ProfileDeclaration.h" //THIS MUST BE CHANGED
#include<windows.h>
#include<string>
#include<fstream>
#include<iomanip>

/*
This function accepts a past memory usage percent value, then
returns the current memory usage minus that previous value.
*/
int getMem(int pastMem);
/*
This function records the memory usage in the passed output file stream.
*/
int recordMem(std::ofstream& outfile, int pastMem, std::string mark);
/*
This function calculates cpu load by using an average over a short period of time.
Since modern processors usually perform tasks in bursts, the average over time must be
used to get accurate cpu usage values.
It first divides the idle ticks by the total ticks, the subtracts that from the full CPU usage value
So if the total ticks are high and idle ticks are low the cpu usage will be high.
If the value it receives is not greater than zero it just returns zero for the calcCPULoad.

This is Jeremy Friesner's code that I added comments to.
*/
static float CalculateCPULoad(unsigned long long idleTicks, unsigned long long totalTicks);

/*
This function converts the filetime struct to a 64 bit integer time.
It takes the upper 32 bit (dw.HighDateTime) and shifts it left 32 bit to store
it in the upper bits of the unsigned long long.  Then the lower 32 bit
(dwLowDateTime) is bitwise or with the upper 32 bits so both values become
stored in the returned unsigned long long (a 64 bit integer).
By converting the three filetime structs in getCPU to 64 bit integer
the calculateCPULoad function can be called to determine the cpu usage

This is Jeremy Friesner's code which I did not modify.
I only moved the { } to make the code more readable.
*/
static unsigned long long FileTimeToInt64(const FILETIME & ft);

// Jeremy Friesner's comments:
// Returns 1.0f for "CPU fully pinned", 0.0f for "CPU idle", or somewhere in between
// You'll need to call this at regular intervals, since it measures the load between
// the previous call and the current one.  Returns -1.0 on error.

/*My comments:
This function returns the current cpu usage of the processor.
It relies on the other functions in this file.
By making filetime structs for the three different times the
cpu usage can be more accurately calculated.
The input integer is assumed to be the previous cpu usage.
This allows for the "load" of a program to be determined
by subtracting the idle usage from the current usage.

This function was modified by me for the purposes of our illuminator project.
The orignal code is by Jeremy Friesner.

More detailed explaination of how code works:
int temp makes a temporary integer.
3 filetime structs are created which hold system timing information.
GetSystemTimes retrieves the idle time, kernel time, and user time.
Then CalculateCPULoad is called which returns the cpu load as described above.
In this case total time is the kernal time plus user time.
If that result is an error than -1 is returned.
Then my modified part uses the pastCPU value to determine the "load" of a program.
Basically, find the idle usage then subtract that usage from any measuresments after starting a stress test.
*/
int getCPU(int pastCPU);

/*
This function records the cpu usage in the passed output file stream.
*/
int recordCPU(std::ofstream& outfile, int pastCPU, std::string mark);

/*
This function accepts a profile class as its input.
It then measures the cpu load and determines what color
the screen should be based on that cpu load.
*/
std::string setColor(Profile pro);
