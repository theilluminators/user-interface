#ifndef LOADMONITOREXECUTION_H
#define LOADMONITOREXECUTION_H

#include "ProfileDeclaration.h"

void monitorLoad(Profile, std::string);

#endif // LOADMONITOREXECUTION_H
