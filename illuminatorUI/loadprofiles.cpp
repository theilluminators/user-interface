#include "loadprofiles.h"
#include "ui_loadprofiles.h"
#include "ProfileDeclaration.h"
#include "AdditionalFunctions.h"
#include "error_dialog.h"
#include "passwordprompt.h"

loadProfiles::loadProfiles(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::loadProfiles)
{
    ui->setupUi(this);
}

loadProfiles::~loadProfiles()
{
    delete ui;
}

void loadProfiles::on_loadProfile_cancelButton_clicked()
{
    canceled=true;
    this->close();
}

void loadProfiles::setValues(std::vector<std::string> profiles)
{
    for(unsigned int i=0; i<profiles.size(); i++)
        ui->loadProfileList->addItem(QString::fromStdString(profiles.at(i)));
}

Profile loadProfiles::getChosenProfile()
{
    return selectedUser;
}

void loadProfiles::on_loadProfile_okButton_clicked()
{
    std::string selectedProfile=ui->loadProfileList->currentItem()->text().toStdString();

    //The alert box to be used for password validity, not the same the prompt uses for input sanitation
    error_dialog *badPass = new error_dialog;
    passwordPrompt *prompt = new passwordPrompt;
    //Help the user by letting them see who's password they are trying to enter
    //Is reset every time the OK button on the load profile ui is pressed
    prompt->setGreeting(selectedProfile);
    //Show the password prompt, only returns after a valid password is entered
    //OR if cancel is clicked!
    prompt->exec();
    if(prompt->canceled) //The prompt was canceled! There is nothing to do!
    {
        return; //Nothing else to do, let the user pick a different profile
    }
    else
    {
        //Get the entered password
        std::string inputPassword=prompt->getInput();
        if(CheckPassword(selectedProfile, inputPassword))
        {
            badPass->setMessage("Password Accepted, welcome back, "+selectedProfile+".");
            badPass->exec();
        }
        else
        {
            badPass->setMessage("Incorrect Password.");
            badPass->exec();
            return; //Password was wrong, let the user choose a different profile or try again
        }
        //Profile correctly selected!
        //Set the selected profile as the active profile and close the dialog
        selectedUser=GetProfile(selectedProfile);
        canceled=false; //The only way to exit from this UI without force closing or pressing cancel!
        this->close();
    }
}
