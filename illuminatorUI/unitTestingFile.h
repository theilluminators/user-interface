#ifndef UNITTESTINGFILE_H
#define UNITTESTINGFILE_H
#include <vector>
#include <string>

std::vector<std::string> getProfiles();

#endif // UNITTESTINGFILE_H
