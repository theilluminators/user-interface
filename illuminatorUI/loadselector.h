#ifndef LOADSELECTOR_H
#define LOADSELECTOR_H

#include <QDialog>
#include "ProfileDeclaration.h"

#define highLoadFilePath "C:\benchmark.exe"
#define lowLoadFilePath "C:\Program Files\Adobe\Adobe Photoshop CC (64 Bit)\Photoshop.exe"

namespace Ui {
class loadSelector;
}

class loadSelector : public QDialog
{
    Q_OBJECT

public:
    explicit loadSelector(QWidget *parent = 0);
    ~loadSelector();
    Profile currentProfile;
    std::string filePath;
    //const std::string highLoadFilePath="C:\benchmark.exe";
    //const std::string lowLoadFilePath="C:\Program Files\Adobe\Adobe Photoshop CC (64 Bit)\Photoshop.exe";

private slots:
    void on_pushButton_clicked();

    void on_highLoadButton_clicked();

    void on_lowLoadButton_clicked();

    void on_customLoadButton_clicked();

private:
    Ui::loadSelector *ui;
};

#endif // LOADSELECTOR_H
