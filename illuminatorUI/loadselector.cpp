#include "loadselector.h"
#include "ui_loadselector.h"
#include "error_dialog.h"
#include "fileprompt.h"
#include "loadMonitorExecution.h"

loadSelector::loadSelector(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loadSelector)
{
    ui->setupUi(this);
}

loadSelector::~loadSelector()
{
    delete ui;
}

//RENAMED BUTTON THIS IS USELESS BUT REMOVING IT CAUSES ERRORS....
void loadSelector::on_pushButton_clicked()//RENAMED BUTTON THIS IS USELESS BUT REMOVING IT CAUSES ERRORS....
{//RENAMED BUTTON THIS IS USELESS BUT REMOVING IT CAUSES ERRORS....
//RENAMED BUTTON THIS IS USELESS BUT REMOVING IT CAUSES ERRORS....
}//RENAMED BUTTON THIS IS USELESS BUT REMOVING IT CAUSES ERRORS....
//RENAMED BUTTON THIS IS USELESS BUT REMOVING IT CAUSES ERRORS....

void loadSelector::on_highLoadButton_clicked()
{
    this->filePath=this->lowLoadFilePath;
    monitorLoad(currentProfile, filePath);
}

void loadSelector::on_lowLoadButton_clicked()
{
    this->filePath=this->highLoadFilePath;
    monitorLoad(currentProfile, filePath);
}

void loadSelector::on_customLoadButton_clicked()
{
    filePrompt *prompt = new filePrompt;
    prompt->setPath(this->filePath);
    prompt->setProfile(currentProfile);
    prompt->exec();
    this->filePath=prompt->filePath;
    monitorLoad(currentProfile, filePath);
}
