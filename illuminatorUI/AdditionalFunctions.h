#ifndef ADDITIONALFUNCTIONS_H
#define ADDITIONALFUNCTIONS_H

#include<ProfileDeclaration.h>

void uploadFile();
void downloadFile();
std::vector<std::string> GetProfiles();
bool CheckPassword(std::string,std::string);
std::string SaveProfile(Profile);
Profile GetProfile(std::string);

#endif // ADDITIONALFUNCTIONS_H
