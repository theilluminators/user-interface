#include "fileprompt.h"
#include "ui_fileprompt.h"
#include <qfiledialog.h>
#include "error_dialog.h"
#include "exehistorybrowser.h"

filePrompt::filePrompt(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::filePrompt)
{
    ui->setupUi(this);
}

filePrompt::~filePrompt()
{
    delete ui;
}

void filePrompt::on_browseButton_clicked()
{
    ui->pathInput->setText(QFileDialog::getOpenFileName());
}

void filePrompt::on_okButton_clicked()
{
    std::string path=ui->pathInput->text().toStdString();
    error_dialog *badInput = new error_dialog;
    if(path == "") //no blank input allowed
    {
        badInput->setMessage("No blank input allowed!");
        badInput->exec();
        return;
    }
    /* Blank spaces are allowed
    for(int i=0; i<path.size(); i++) //check for illegal characters
    {
        if(path.at(i) == ' ') //check for blank spaces
        {
            badInput->setMessage("No blankspace characters are allowed!");
            badInput->exec();
            return;
        }
    }
    */
    if(path.at(path.size()-3)!='e' || path.at(path.size()-2)!='x' || path.at(path.size()-1)!='e') //check the file extension is "exe"
    {
        badInput->setMessage("File must be an executable!");
        badInput->exec();
        return;
    }
    badInput->setMessage("File path will be saved in "+currProfile.get_name()+"'s exe history");
    badInput->exec();
    filePath = ui->pathInput->text().toStdString();
    currProfile.appendToRecentExes(filePath);
    this->close();
}

void filePrompt::setPath(std::string chosenPath)
{
    ui->pathInput->setText(QString::fromStdString(chosenPath));
}

void filePrompt::on_historyButton_clicked()
{
    exeHistoryBrowser *historyUI = new exeHistoryBrowser;
    std::vector<std::string> temp = currProfile.get_recentExes();
    if(temp.size()==0)
    {
        error_dialog *errDialog = new error_dialog;
        errDialog->setMessage("No exes in history!");
        errDialog->exec();
    }
    else
    {
        historyUI->setFilePaths(temp);
        historyUI->exec();
    }
}

void filePrompt::setProfile(Profile temp)
{
    currProfile=temp;
}
