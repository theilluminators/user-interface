#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QDialog>

namespace Ui {
class colorPicker;
}

class colorPicker : public QDialog
{
    Q_OBJECT

public:
    explicit colorPicker(QDialog *parent = 0);
    ~colorPicker();
    std::string chosenColor;

private slots:
    void on_okButton_clicked();

private:
    Ui::colorPicker *ui;
};

#endif // COLORPICKER_H
