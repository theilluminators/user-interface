#ifndef CREATEPROFILE_H
#define CREATEPROFILE_H

#include <QDialog>
#include "ProfileDeclaration.h"

namespace Ui {
class createProfile;
}

class createProfile : public QDialog
{
    Q_OBJECT

public:
    explicit createProfile(QDialog *parent = 0);
    ~createProfile();
    //In case the ui is canceled or force closed
    bool canceled=true;
    std::vector<std::string> colors;
    std::vector<int> threshStart;
    std::vector<int> threshEnd;
    Profile newProfile;

private slots:
    void on_endSpinBox_1_valueChanged(int arg1);

    void on_startSpinBox_2_valueChanged(int arg1);

    void on_endSpinBox_2_valueChanged(const QString &arg1);

    void on_endSpinBox_1_editingFinished();

    void on_startSpinBox_2_editingFinished();

    void on_endSpinBox_2_editingFinished();

    void on_startSpinBox_3_valueChanged(int arg1);

    void on_endSpinBox_3_valueChanged(const QString &arg1);

    void on_startSpinBox_4_valueChanged(const QString &arg1);

    void on_threshSpinBox_editingFinished();

    void on_endSpinBox_4_valueChanged(const QString &arg1);

    void on_startSpinBox_5_valueChanged(const QString &arg1);

    void on_endSpinBox_5_valueChanged(const QString &arg1);

    void on_startSpinBox_6_valueChanged(const QString &arg1);

    void on_endSpinBox_6_valueChanged(int arg1);

    void on_startSpinBox_7_valueChanged(int arg1);

    void on_endSpinBox_7_valueChanged(int arg1);

    void on_startSpinBox_8_valueChanged(int arg1);

    void on_threshSpinBox_valueChanged(const QString &arg1);

    void on_cancelButton_clicked();

    void on_colorButton_1_clicked();

    void on_colorButton_2_clicked();

    void on_colorButton_3_clicked();

    void on_colorButton_4_clicked();

    void on_colorButton_5_clicked();

    void on_colorButton_6_clicked();

    void on_colorButton_7_clicked();

    void on_colorButton_8_clicked();

    void on_okButton_clicked();

private:
    Ui::createProfile *ui;
};

#endif // CREATEPROFILE_H
