/*
 *
 * This is the ui for choosing a single profile from the list of profiles.
 * After selecting a profile, a password prompt to verify the user has access
 * is shown. Only after correctly entering a password, or pressing cancel is
 * it hidden.
 *
 */

#ifndef LOADPROFILES_H
#define LOADPROFILES_H

#include <QDialog>
#include "ProfileDeclaration.h"

namespace Ui {
class loadProfiles;
}

class loadProfiles : public QDialog
{
    Q_OBJECT

public:
    explicit loadProfiles(QDialog *parent = 0);
    ~loadProfiles();
    void setValues(std::vector<std::string>);
    Profile getChosenProfile();
    //How to tell if valid profile is loaded
    //or if the ui was forcefully quit/canceled
    bool canceled=true;
private slots:
    void on_loadProfile_cancelButton_clicked();

    void on_loadProfile_okButton_clicked();

private:
    Ui::loadProfiles *ui;
    Profile selectedUser;
};

#endif // LOADPROFILES_H
