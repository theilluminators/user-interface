#ifndef FILEPROMPT_H
#define FILEPROMPT_H

#include <QDialog>
#include "ProfileDeclaration.h"

namespace Ui {
class filePrompt;
}

class filePrompt : public QDialog
{
    Q_OBJECT

public:
    explicit filePrompt(QWidget *parent = 0);
    ~filePrompt();
    std::string filePath;
    Profile currProfile;
    void setPath(std::string);
    void setProfile(Profile);

private slots:
    void on_browseButton_clicked();

    void on_okButton_clicked();

    void on_historyButton_clicked();

private:
    Ui::filePrompt *ui;
};

#endif // FILEPROMPT_H
