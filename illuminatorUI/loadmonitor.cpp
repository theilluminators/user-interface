#include "loadmonitor.h"
#include "ui_loadmonitor.h"
#include "error_dialog.h"

loadMonitor::loadMonitor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loadMonitor)
{
    ui->setupUi(this);
    hasQuit=false;
}

loadMonitor::~loadMonitor()
{
    delete ui;
}

void loadMonitor::on_quitButton_clicked()
{
    hasQuit=true;
}

bool loadMonitor::saveBoxChecked()
{
    return ui->saveBox->isChecked();
}

void loadMonitor::on_saveButton_clicked()
{
    feedback = ui->feedbackInput->text().toStdString();
    if(feedback=="") //show a warning, but don't stop anything
    {
        error_dialog *oddInput = new error_dialog;
        oddInput->setMessage("Entering blank input has not point....");
        oddInput->show(); //Using show does not pause execution!
    }
}

std::string loadMonitor::getFeedback()
{
    std::string temp = feedback;
    feedback = ""; //Clear feedback to prevent extraneous output in file
    return temp;
}
