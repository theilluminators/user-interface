#include "createprofile.h"
#include "ui_createprofile.h"
#include "colorpicker.h"
#include "error_dialog.h"
#include "AdditionalFunctions.h"
#include "ProfileDeclaration.h"

createProfile::createProfile(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::createProfile)
{
    ui->setupUi(this);
    ui->threshValFrame_8->setDisabled(true);
    ui->threshValFrame_7->setDisabled(true);
    ui->threshValFrame_6->setDisabled(true);
    ui->threshValFrame_5->setDisabled(true);
    ui->threshValFrame_4->setDisabled(true);
    ui->threshValFrame_3->setDisabled(true);
    ui->threshValFrame_2->setDisabled(true);
    ui->endSpinBox_1->setValue(100);
    for(int i=0; i<8; i++) //Initialize the color vector
    {
        if(colors.size()<8) //Check, in case the ui is closed and reopened
            colors.push_back("red");
        if(threshStart.size()<8) //Check, in case the ui is closed and reopened
            threshStart.push_back(0);
        if(threshEnd.size()<8) //Check, in case the ui is closed and reopened
            threshEnd.push_back(0);
    }
    ui->passInput->setEchoMode(QLineEdit::Password);
}

createProfile::~createProfile()
{
    delete ui;
}

void createProfile::on_threshSpinBox_editingFinished()
{

}

void createProfile::on_endSpinBox_1_valueChanged(int arg1)
{
    //Make sure the end values is not less than the start value!
    if(ui->endSpinBox_1->value() <= ui->startSpinBox_1->value())
        ui->endSpinBox_1->setValue(ui->startSpinBox_1->value()+1);
    //The next threshold should start where this one ends
    if(ui->threshSpinBox->value() > 1) //If this is not the last threshold, modify the starting point of the next threshold
    {
            ui->startSpinBox_2->setValue(ui->endSpinBox_1->value()+1);
        if(ui->startSpinBox_2->value() <= ui->endSpinBox_1->value())
            ui->endSpinBox_1->setValue(ui->startSpinBox_2->value()-1);
    }
    else //If the ending threshold, end value HAS to be 100
    {
        ui->endSpinBox_1->setValue(100);
    }
}

void createProfile::on_startSpinBox_2_valueChanged(int arg1)
{
    //Start value should not be greater than end value
    if(ui->startSpinBox_2->value() >= ui->endSpinBox_2->value())
        ui->startSpinBox_2->setValue(ui->endSpinBox_2->value()-1);
    ui->endSpinBox_1->setValue(ui->startSpinBox_2->value()-1);
}

void createProfile::on_endSpinBox_2_valueChanged(const QString &arg1)
{
    //Make sure the end values is not less than the start value!
    if(ui->endSpinBox_2->value() <= ui->startSpinBox_2->value())
        ui->endSpinBox_2->setValue(ui->startSpinBox_2->value()+1);
    //The next threshold should start where this one ends
    if(ui->threshSpinBox->value() > 2) //If this is not the last threshold, modify the starting point of the next threshold
    {
            ui->startSpinBox_3->setValue(ui->endSpinBox_2->value()+1);
        if(ui->startSpinBox_3->value() <= ui->endSpinBox_2->value())
            ui->endSpinBox_2->setValue(ui->startSpinBox_3->value()-1);
    }
    else //If the ending threshold, end value HAS to be 100
    {
        ui->endSpinBox_2->setValue(100);
    }
}

void createProfile::on_endSpinBox_1_editingFinished()
{

}

void createProfile::on_startSpinBox_2_editingFinished()
{

}

void createProfile::on_endSpinBox_2_editingFinished()
{

}

void createProfile::on_startSpinBox_3_valueChanged(int arg1)
{
    //Start value should not be greater than end value
    if(ui->startSpinBox_3->value() >= ui->endSpinBox_3->value())
        ui->startSpinBox_3->setValue(ui->endSpinBox_3->value()-1);
    ui->endSpinBox_2->setValue(ui->startSpinBox_3->value()-1);
}

void createProfile::on_endSpinBox_3_valueChanged(const QString &arg1)
{
    //Make sure the end values is not less than the start value!
    if(ui->endSpinBox_3->value() <= ui->startSpinBox_3->value())
        ui->endSpinBox_3->setValue(ui->startSpinBox_3->value()+1);
    //The next threshold should start where this one ends
    if(ui->threshSpinBox->value() > 3) //If this is not the last threshold, modify the starting point of the next threshold
    {
            ui->startSpinBox_4->setValue(ui->endSpinBox_3->value()+1);
        if(ui->startSpinBox_4->value() <= ui->endSpinBox_3->value())
            ui->endSpinBox_3->setValue(ui->startSpinBox_4->value()-1);
    }
    else //If the ending threshold, end value HAS to be 100
    {
        ui->endSpinBox_3->setValue(100);
    }
}

void createProfile::on_startSpinBox_4_valueChanged(const QString &arg1)
{
    //Start value should not be greater than end value
    if(ui->startSpinBox_4->value() >= ui->endSpinBox_4->value())
        ui->startSpinBox_4->setValue(ui->endSpinBox_4->value()-1);
    ui->endSpinBox_3->setValue(ui->startSpinBox_4->value()-1);
}

void createProfile::on_endSpinBox_4_valueChanged(const QString &arg1)
{
    //Make sure the end values is not less than the start value!
    if(ui->endSpinBox_4->value() <= ui->startSpinBox_4->value())
        ui->endSpinBox_4->setValue(ui->startSpinBox_4->value()+1);
    //The next threshold should start where this one ends
    if(ui->threshSpinBox->value() > 4) //If this is not the last threshold, modify the starting point of the next threshold
    {
            ui->startSpinBox_5->setValue(ui->endSpinBox_4->value()+1);
        if(ui->startSpinBox_5->value() <= ui->endSpinBox_4->value())
            ui->endSpinBox_4->setValue(ui->startSpinBox_5->value()-1);
    }
    else //If the ending threshold, end value HAS to be 100
    {
        ui->endSpinBox_4->setValue(100);
    }
}

void createProfile::on_startSpinBox_5_valueChanged(const QString &arg1)
{
    //Start value should not be greater than end value
    if(ui->startSpinBox_5->value() >= ui->endSpinBox_5->value())
        ui->startSpinBox_5->setValue(ui->endSpinBox_5->value()-1);
    ui->endSpinBox_4->setValue(ui->startSpinBox_5->value()-1);
}

void createProfile::on_endSpinBox_5_valueChanged(const QString &arg1)
{
    //Make sure the end values is not less than the start value!
    if(ui->endSpinBox_5->value() <= ui->startSpinBox_5->value())
        ui->endSpinBox_5->setValue(ui->startSpinBox_5->value()+1);
    //The next threshold should start where this one ends
    if(ui->threshSpinBox->value() > 5) //If this is not the last threshold, modify the starting point of the next threshold
    {
            ui->startSpinBox_6->setValue(ui->endSpinBox_5->value()+1);
        if(ui->startSpinBox_6->value() <= ui->endSpinBox_5->value())
            ui->endSpinBox_5->setValue(ui->startSpinBox_6->value()-1);
    }
    else //If the ending threshold, end value HAS to be 100
    {
        ui->endSpinBox_5->setValue(100);
    }
}

void createProfile::on_startSpinBox_6_valueChanged(const QString &arg1)
{
    //Start value should not be greater than end value
    if(ui->startSpinBox_6->value() >= ui->endSpinBox_6->value())
        ui->startSpinBox_6->setValue(ui->endSpinBox_6->value()-1);
    ui->endSpinBox_5->setValue(ui->startSpinBox_6->value()-1);
}

void createProfile::on_endSpinBox_6_valueChanged(int arg1)
{
    //Make sure the end values is not less than the start value!
    if(ui->endSpinBox_6->value() <= ui->startSpinBox_6->value())
        ui->endSpinBox_6->setValue(ui->startSpinBox_6->value()+1);
    //The next threshold should start where this one ends
    if(ui->threshSpinBox->value() > 6) //If this is not the last threshold, modify the starting point of the next threshold
    {
            ui->startSpinBox_7->setValue(ui->endSpinBox_6->value()+1);
        if(ui->startSpinBox_7->value() <= ui->endSpinBox_6->value())
            ui->endSpinBox_6->setValue(ui->startSpinBox_7->value()-1);
    }
    else //If the ending threshold, end value HAS to be 100
    {
        ui->endSpinBox_6->setValue(100);
    }
}

void createProfile::on_startSpinBox_7_valueChanged(int arg1)
{
    //Start value should not be greater than end value
    if(ui->startSpinBox_7->value() >= ui->endSpinBox_7->value())
        ui->startSpinBox_7->setValue(ui->endSpinBox_7->value()-1);
    ui->endSpinBox_6->setValue(ui->startSpinBox_7->value()-1);
}

void createProfile::on_endSpinBox_7_valueChanged(int arg1)
{
    //Make sure the end values is not less than the start value!
    if(ui->endSpinBox_7->value() <= ui->startSpinBox_7->value())
        ui->endSpinBox_7->setValue(ui->startSpinBox_7->value()+1);
    //The next threshold should start where this one ends
    if(ui->threshSpinBox->value() > 6) //If this is not the last threshold, modify the starting point of the next threshold
    {
            ui->startSpinBox_8->setValue(ui->endSpinBox_8->value()+1);
        if(ui->startSpinBox_8->value() <= ui->endSpinBox_8->value())
            ui->endSpinBox_7->setValue(ui->startSpinBox_8->value()-1);
    }
    else //If the ending threshold, end value HAS to be 100
    {
        ui->endSpinBox_7->setValue(100);
    }
}

void createProfile::on_startSpinBox_8_valueChanged(int arg1)
{
    //Start value should not be greater than end value
    if(ui->startSpinBox_8->value() >= ui->endSpinBox_8->value())
        ui->startSpinBox_8->setValue(ui->endSpinBox_8->value()-1);
    ui->endSpinBox_7->setValue(ui->startSpinBox_8->value()-1);
}

//After changing the number of thresholds, the appropriate frames should be disabled,
//and the start/end values should be updated.
void createProfile::on_threshSpinBox_valueChanged(const QString &arg1)
{
    switch (ui->threshSpinBox->value()) {
    case 8:
        ui->threshValFrame_8->setDisabled(false);
        ui->threshValFrame_7->setDisabled(false);
        ui->threshValFrame_6->setDisabled(false);
        ui->threshValFrame_5->setDisabled(false);
        ui->threshValFrame_4->setDisabled(false);
        ui->threshValFrame_3->setDisabled(false);
        ui->threshValFrame_2->setDisabled(false);
        ui->endSpinBox_8->setValue(100);
        on_startSpinBox_8_valueChanged(100);
        break;
    case 7:
        ui->threshValFrame_8->setDisabled(true);
        ui->threshValFrame_7->setDisabled(false);
        ui->threshValFrame_6->setDisabled(false);
        ui->threshValFrame_5->setDisabled(false);
        ui->threshValFrame_4->setDisabled(false);
        ui->threshValFrame_3->setDisabled(false);
        ui->threshValFrame_2->setDisabled(false);
        ui->endSpinBox_7->setValue(100);
        on_startSpinBox_7_valueChanged(100);
        break;
    case 6:
        ui->threshValFrame_8->setDisabled(true);
        ui->threshValFrame_7->setDisabled(true);
        ui->threshValFrame_6->setDisabled(false);
        ui->threshValFrame_5->setDisabled(false);
        ui->threshValFrame_4->setDisabled(false);
        ui->threshValFrame_3->setDisabled(false);
        ui->threshValFrame_2->setDisabled(false);
        ui->endSpinBox_6->setValue(100);
        on_startSpinBox_6_valueChanged(QString::fromStdString("100"));
        break;
    case 5:
        ui->threshValFrame_8->setDisabled(true);
        ui->threshValFrame_7->setDisabled(true);
        ui->threshValFrame_6->setDisabled(true);
        ui->threshValFrame_5->setDisabled(false);
        ui->threshValFrame_4->setDisabled(false);
        ui->threshValFrame_3->setDisabled(false);
        ui->threshValFrame_2->setDisabled(false);
        ui->endSpinBox_5->setValue(100);
        on_startSpinBox_5_valueChanged(QString::fromStdString("100"));
        break;
    case 4:
        ui->threshValFrame_8->setDisabled(true);
        ui->threshValFrame_7->setDisabled(true);
        ui->threshValFrame_6->setDisabled(true);
        ui->threshValFrame_5->setDisabled(true);
        ui->threshValFrame_4->setDisabled(false);
        ui->threshValFrame_3->setDisabled(false);
        ui->threshValFrame_2->setDisabled(false);
        ui->endSpinBox_4->setValue(100);
        on_startSpinBox_4_valueChanged(QString::fromStdString("100"));
        break;
    case 3:
        ui->threshValFrame_8->setDisabled(true);
        ui->threshValFrame_7->setDisabled(true);
        ui->threshValFrame_6->setDisabled(true);
        ui->threshValFrame_5->setDisabled(true);
        ui->threshValFrame_4->setDisabled(true);
        ui->threshValFrame_3->setDisabled(false);
        ui->threshValFrame_2->setDisabled(false);
        ui->endSpinBox_3->setValue(100);
        on_startSpinBox_3_valueChanged(100);
        break;
    case 2:
        ui->threshValFrame_8->setDisabled(true);
        ui->threshValFrame_7->setDisabled(true);
        ui->threshValFrame_6->setDisabled(true);
        ui->threshValFrame_5->setDisabled(true);
        ui->threshValFrame_4->setDisabled(true);
        ui->threshValFrame_3->setDisabled(true);
        ui->threshValFrame_2->setDisabled(false);
        ui->endSpinBox_2->setValue(100);
        on_startSpinBox_2_valueChanged(100);
        break;
    case 1:
        ui->threshValFrame_8->setDisabled(true);
        ui->threshValFrame_7->setDisabled(true);
        ui->threshValFrame_6->setDisabled(true);
        ui->threshValFrame_5->setDisabled(true);
        ui->threshValFrame_4->setDisabled(true);
        ui->threshValFrame_3->setDisabled(true);
        ui->threshValFrame_2->setDisabled(true);
        ui->endSpinBox_1->setValue(100);
        ui->startSpinBox_1->setValue(0);
        break;
    default:
        break;
    }
}

void createProfile::on_cancelButton_clicked()
{
    canceled=true;
    this->close();
}

void createProfile::on_colorButton_1_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(0)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(0);
    ui->colorButton_1->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_colorButton_2_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(1)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(1);
    ui->colorButton_2->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_colorButton_3_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(2)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(2);
    ui->colorButton_3->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_colorButton_4_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(3)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(3);
    ui->colorButton_4->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_colorButton_5_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(4)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(4);
    ui->colorButton_5->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_colorButton_6_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(5)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(5);
    ui->colorButton_6->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_colorButton_7_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(6)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(6);
    ui->colorButton_7->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_colorButton_8_clicked()
{
    colorPicker *chooseColor = new colorPicker;
    chooseColor->exec();
    colors.at(7)=chooseColor->chosenColor;
    std::string colorSet = "background-color:"+colors.at(7);
    ui->colorButton_8->setStyleSheet(QString::fromStdString(colorSet));
}

void createProfile::on_okButton_clicked()
{
    error_dialog *invalidInput = new error_dialog;
    std::string errors="";

    bool badInput = false;

    //Input sanitation on the name
    std::string nameInput=ui->nameInput->text().toStdString();
    if(nameInput=="")
    {
        badInput=true;
        errors+="Blank input is not allowed in name\n";
    }
    //check for illegal characters
    for(unsigned int i=0; i<nameInput.size(); i++)
    {
        if(nameInput.at(i)==' ')
        {
            errors+="No blank characters allowed in name input\n";
            i=nameInput.size();
            badInput=true;
        }
    }

    //Input sanitation on the password
    std::string passInput=ui->passInput->text().toStdString();
    if(passInput=="")
    {
        errors+="Blank input is not allowed in password\n";
        badInput=true;
    }
    //check for illegal characters
    for(unsigned int i=0; i<passInput.size(); i++)
    {
        if(passInput.at(i)==' ')
        {
            errors+="No blank characters allowed in password input\n";
            i=passInput.size();
            badInput=true;
        }
    }

    //Don't save the profile if invalid input was made
    if(badInput)
    {
        invalidInput->setMessage(errors);
        invalidInput->exec();
        return;
    }
    switch (ui->threshSpinBox->value()) {
    case 8:
        threshStart.at(7)=ui->startSpinBox_8->value();
        threshEnd.at(7)=ui->endSpinBox_8->value();
    case 7:
        threshStart.at(6)=ui->startSpinBox_7->value();
        threshEnd.at(6)=ui->endSpinBox_7->value();
    case 6:
        threshStart.at(5)=ui->startSpinBox_6->value();
        threshEnd.at(5)=ui->endSpinBox_6->value();
    case 5:
        threshStart.at(4)=ui->startSpinBox_5->value();
        threshEnd.at(4)=ui->endSpinBox_5->value();
    case 4:
        threshStart.at(3)=ui->startSpinBox_4->value();
        threshEnd.at(3)=ui->endSpinBox_4->value();
    case 3:
        threshStart.at(2)=ui->startSpinBox_3->value();
        threshEnd.at(2)=ui->endSpinBox_3->value();
    case 2:
        threshStart.at(1)=ui->startSpinBox_2->value();
        threshEnd.at(1)=ui->endSpinBox_2->value();
    case 1:
        threshStart.at(0)=ui->startSpinBox_1->value();
        threshEnd.at(0)=ui->endSpinBox_1->value();
        break;
    }
    Profile temp(ui->nameInput->text().toStdString(),ui->passInput->text().toStdString(),ui->threshSpinBox->value(),threshStart,threshEnd,colors);
    error_dialog *saveStatus = new error_dialog;
    saveStatus->setMessage(SaveProfile(temp));
    newProfile=temp;
    this->close();
}
