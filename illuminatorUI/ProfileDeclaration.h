//Written By: Brandon Busuttil
//Date: 4/15/2015
//Description: Header file containing the Profile class, and the functions necessary for changing, extracting, and creating Profile types.

#ifndef PROFILE_H
#define PROFILE_H
#include <string>
#include <vector>

using namespace std;

class Profile
{
public:
	int get_ThreshStart(int num);				//Inputs a integer of the threshold number, and returns the starting threshold percentage at that threshold
	int get_ThreshEnd(int num);					//Inputs integer value threshold number, and returns the end percentage at that threshold
	int get_NumThresholds();					//Returns the number of threshold divisons
	string get_color(int thresholdNum);			//Returns color for the specified threshold
	string get_name();							//Returns Name private variable
	string get_password();						//Returns Password private variable

	Profile();									//Default constructor
	Profile(string name, string pass,int numThresh, vector<int> threshstart, vector<int>threshEnd, vector<string> colors);//Constructor
    //Profile(const Profile &obj);                //Copy Constructor
	void set_name(string Strin);				//Sets the name of the profile
	void set_password(string Strin);			//Sets The password
	void set_numThresholds(int num);			//Sets the number of thresholds
	vector<string> get_recentExes();			//Return the recentExes private variable
	void appendToRecentExes(string Strin);		//Adds the string input variable to the private variable

private:
	int numThresholds;						//Number of threshold divisons
	string Name;							//Name of profile
	string Password;						//Holds password for profile
	vector<string> recentExes;				//Variable for holding recent exes file names
	vector<int> ThresholdStart;				//Variable for holding starting thresholds percent values
	vector<int> ThresholdEnd;				//Variable for holding ending thresholds percent values
	vector<string> colors;					//Variable for holding colors

};

#endif
