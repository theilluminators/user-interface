/*
CPU and Mem usage header file
Some original code by: Jeremy Friesner (each function he made is marked)
https://stackoverflow.com/questions/23143693/retrieving-cpu-load-percent-total-in-windows-with-c

Coded by: Ryan Dean
Changelog:
4/22/2015 - Tested original code for functionality
4/23/2015 - Changed getCPU function to comply with detail design
4/29/2015 - Wrote even more comments, made memory usage function, made mem and cpu records function,
            made setColor function.

This code is responsible for all cpu usage and memory usage functions.
GetSystemTimes and GETMEMORYSTATUSEX are both microsoft windows api functions, which is
where I get the cpu and memory usage from.

While some of the CPU code is from Jeremy Friesner I made 100% sure that I completely understand
how it works by doing extensive research.  Hopefully my explainations are clear.
*/

#include"ProfileDeclaration.h" //THIS MUST BE CHANGED
#include<windows.h>
#include<string>
#include<fstream>
#include<iomanip>

/*
This function accepts a past memory usage percent value, then
returns the current memory usage minus that previous value.
*/
int getMem(int pastMem)
{
    int temp=0; //create a temporary int
    MEMORYSTATUSEX statex; //declare a MEMORYSTATUSEX struct
    statex.dwLength = sizeof (statex);	//must define the size of the struct before using GlobalMemoryStatusEx function

    GlobalMemoryStatusEx(&statex); //this function retrieves the systems current info about physical and virtual memory

    temp = (int) statex.dwMemoryLoad; //make temp equal to the current system memory load

    return ((temp - pastMem) > 0) ? (temp - pastMem) : 0; //use the past memory usage to determine the load of a program
}

/*
This function records the memory usage in the passed output file stream.
*/
int recordMem(std::ofstream& outfile, int pastMem, std::string mark)
{
    int mem=0; //temporary int to hold mem percent
    SYSTEMTIME local; //SYSTEMTIME struct

    mem = getMem(pastMem); //call getMem to retrieve the memory usage percent
    GetLocalTime(&local); //call GetLocalTime to retrieve the current local time

    //output all of these values to the output stream (should be a csv file), mark string can be used to mark a time
    outfile << setfill('0') << setw(2) << local.wMinute << setw(2) << local.wSecond << setw(3) << local.wMilliseconds << "," << mem << "," << mark << std::endl;
    return mem;
}
/*
This function calculates cpu load by using an average over a short period of time.
Since modern processors usually perform tasks in bursts, the average over time must be
used to get accurate cpu usage values.
It first divides the idle ticks by the total ticks, the subtracts that from the full CPU usage value
So if the total ticks are high and idle ticks are low the cpu usage will be high.
If the value it receives is not greater than zero it just returns zero for the calcCPULoad.

This is Jeremy Friesner's code that I added comments to.
*/
static float CalculateCPULoad(unsigned long long idleTicks, unsigned long long totalTicks)
{
    static unsigned long long _previousTotalTicks = 0; //these are not reintialized because they are static
    static unsigned long long _previousIdleTicks = 0;	//they hold previous values of cpu usage to average out the usage

    unsigned long long totalTicksSinceLastTime = totalTicks - _previousTotalTicks; //subtract previous time to get an "average" which is more accurate
    unsigned long long idleTicksSinceLastTime = idleTicks - _previousIdleTicks;	   //on modern processors that do tasks in bursts

    //divide the idle ticks by the total ticks, the subtract that from the full CPU usage value
    float ret = 1.0f - ((totalTicksSinceLastTime > 0) ? ((float)idleTicksSinceLastTime) / totalTicksSinceLastTime : 0);

    _previousTotalTicks = totalTicks; //set the previous ticks to the current ticks since we are done using them
    _previousIdleTicks = idleTicks;
    return ret;
}

/*
This function converts the filetime struct to a 64 bit integer time.
It takes the upper 32 bit (dw.HighDateTime) and shifts it left 32 bit to store
it in the upper bits of the unsigned long long.  Then the lower 32 bit
(dwLowDateTime) is bitwise or with the upper 32 bits so both values become
stored in the returned unsigned long long (a 64 bit integer).
By converting the three filetime structs in getCPU to 64 bit integer
the calculateCPULoad function can be called to determine the cpu usage

This is Jeremy Friesner's code which I did not modify.
I only moved the { } to make the code more readable.
*/
static unsigned long long FileTimeToInt64(const FILETIME & ft)
{
    return (((unsigned long long)(ft.dwHighDateTime)) << 32) | ((unsigned long long)ft.dwLowDateTime);
}

// Jeremy Friesner's comments:
// Returns 1.0f for "CPU fully pinned", 0.0f for "CPU idle", or somewhere in between
// You'll need to call this at regular intervals, since it measures the load between
// the previous call and the current one.  Returns -1.0 on error.

/*My comments:
This function returns the current cpu usage of the processor.
It relies on the other functions in this file.
By making filetime structs for the three different times the
cpu usage can be more accurately calculated.
The input integer is assumed to be the previous cpu usage.
This allows for the "load" of a program to be determined
by subtracting the idle usage from the current usage.

This function was modified by me for the purposes of our illuminator project.
The orignal code is by Jeremy Friesner.

More detailed explaination of how code works:
int temp makes a temporary integer.
3 filetime structs are created which hold system timing information.
GetSystemTimes retrieves the idle time, kernel time, and user time.
Then CalculateCPULoad is called which returns the cpu load as described above.
In this case total time is the kernal time plus user time.
If that result is an error than -1 is returned.
Then my modified part uses the pastCPU value to determine the "load" of a program.
Basically, find the idle usage then subtract that usage from any measuresments after starting a stress test.
*/
int getCPU(int pastCPU)
{
    int temp=0;
    FILETIME idleTime, kernelTime, userTime; //these are filetime structs, and the next line calculates the cpu usage and converts it to an int between 0 - 100
    //get the System times, then send these values to calculate function after converting them to 64 bit ints, the total ticks is kernal + user time, if there is error return -1
    temp = GetSystemTimes(&idleTime, &kernelTime, &userTime) ? (int)(100 * CalculateCPULoad(FileTimeToInt64(idleTime), FileTimeToInt64(kernelTime) + FileTimeToInt64(userTime))) : -1;

    return ((temp - pastCPU) > 0) ? (temp - pastCPU) : 0; //use the previous cpu usage to determine the "load" of a program, return zero if the answer is negative
}

/*
This function records the cpu usage in the passed output file stream.
*/
int recordCPU(std::ofstream& outfile, int pastCPU, std::string mark)
{
    int cpuu=0; //temporary int to hold cpu percent
    SYSTEMTIME local; //SYSTEMTIME struct

    cpuu = getCPU(pastCPU); //call getCPU to retrieve the memory usage percent
    GetLocalTime(&local); //call GetLocalTime to retrieve the current local time

    //output all of these values to the output stream (should be a csv file), mark string can be used to mark a time
    outfile << setfill('0') << setw(2) << local.wMinute << setw(2) << local.wSecond << setw(3) << local.wMilliseconds << "," << cpuu << "," << mark << std::endl;
    return cpuu;
}

/*
This function accepts a profile class as its input.
It then measures the cpu load and determines what color
the screen should be based on that cpu load.
*/
std::string setColor(Profile pro)
{
    int numThresh=0, cpuu=0; //vaiables to hold the number of thresholds and cpu load
    numThresh = pro.get_NumThresholds(); //obtaining the number of thresholds
    cpuu = getCPU(0); //obtaining the cpu usage

    for (int i = 0; i < numThresh; i++) //check every threshold
    {
        if (cpuu >= pro.get_ThreshStart(i) && cpuu <= pro.get_ThreshEnd(i)) //if the cpu usage is within that threshold
            return pro.get_color(i); //then return the color value of that threshold
    }

    return "error"; //if no color value was returned then return an error (this will most likely never happen)
}

