#ifndef PASSWORDPROMPT_H
#define PASSWORDPROMPT_H

#include <QDialog>

namespace Ui {
class passwordPrompt;
}

class passwordPrompt : public QDialog
{
    Q_OBJECT

public:
    explicit passwordPrompt(QDialog *parent = 0);
    ~passwordPrompt();
    void setGreeting(std::string);
    //Used to get the entered value
    std::string getInput();
    //How to tell whether the prompt was canceled
    //Defaults to true in case the prompt is forcefully closed
    bool canceled=true;

private slots:
    void on_passCancel_clicked();

    void on_passOK_clicked();

private:
    Ui::passwordPrompt *ui;
};

#endif // PASSWORDPROMPT_H
