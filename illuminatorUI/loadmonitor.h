#ifndef LOADMONITOR_H
#define LOADMONITOR_H

#include <QDialog>

namespace Ui {
class loadMonitor;
}

class loadMonitor : public QDialog
{
    Q_OBJECT

public:
    explicit loadMonitor(QWidget *parent = 0);
    ~loadMonitor();
    bool hasQuit;
    bool saveBoxChecked();
    std::string getFeedback();

private slots:
    void on_quitButton_clicked();

    void on_saveButton_clicked();

private:
    std::string feedback;
    Ui::loadMonitor *ui;
};

#endif // LOADMONITOR_H
