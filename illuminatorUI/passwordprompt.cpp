#include "passwordprompt.h"
#include "ui_passwordprompt.h"
#include "error_dialog.h"

passwordPrompt::passwordPrompt(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::passwordPrompt)
{
    ui->setupUi(this);
    ui->passInput->setEchoMode(QLineEdit::Password);
}

passwordPrompt::~passwordPrompt()
{
    delete ui;
}

void passwordPrompt::setGreeting(std::string input)
{
    ui->passwordLabel->setText(QString::fromStdString("Enter Password for "+input+":"));
}

std::string passwordPrompt::getInput()
{
    return ui->passInput->text().toStdString();
}

void passwordPrompt::on_passCancel_clicked()
{
    canceled=true;
    this->close();
}

void passwordPrompt::on_passOK_clicked()
{
    error_dialog *badInput = new error_dialog;
    std::string input = ui->passInput->text().toStdString();
    if(input == "") //check for blank input
    {
        badInput->setMessage("Blank input is not allowed");
        badInput->exec();
        return; //Don't check for anything else
    }
    //If not blank input...
    for(int i=0; i<input.size(); i++) //Check for illegal characters
    {
        if(input.at(i)==' ') //check for blank character
        {
            badInput->setMessage("No blank characters are allowed");
            badInput->exec();
            return;
        }
    }
    //If no illegal characters...
    canceled = false; //The only way to exit other than force close or cancel
    //Valid input, close dialog for profile select ui to check password
    this->close();
}
