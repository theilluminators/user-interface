#ifndef COLORDISPLAY_H
#define COLORDISPLAY_H

#include <QWidget>

namespace Ui {
class colorDisplay;
}

class colorDisplay : public QWidget
{
    Q_OBJECT

public:
    explicit colorDisplay(QWidget *parent = 0);
    ~colorDisplay();

private:
    Ui::colorDisplay *ui;
};

#endif // COLORDISPLAY_H
