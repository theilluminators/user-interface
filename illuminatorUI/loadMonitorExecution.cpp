#include "loadMonitorExecution.h"
#include "loadmonitor.h"
#include "colordisplay.h"
#include "CPUMem.h"
#include "fstream"
#include "error_dialog.h"

void monitorLoad(Profile currUser, std::string filePath)
{
    loadMonitor *loadDisplay = new loadMonitor;
    colorDisplay *loadColor = new colorDisplay;
    error_dialog *warnings = new error_dialog;

    loadDisplay->show();
    loadColor->show();

    warnings->setMessage("Please use the quit button to close these windows,\nto prevent problems with the output file");

    //string to hold user feeback on load

    //File to save records to
    ofstream outputFile;
    //File name is: savedData_[username].txt
    outputFile.open("savedData_"+currUser.get_name()+".txt");

    int startLoad=0;
    if(filePath!="") //Not reading overall load
    {
        startLoad = getCPU(0);
    }

    //Start the exe otherwise, if it fails to load,
    //it'll just be a messed up overall load
    else
    {
        system(filePath.c_str());
    }

    //loop until the user quits
    while(!loadDisplay->hasQuit)
    {
        std::string color;
        color = setColor(currUser);
        loadColor->setStyleSheet(QString::fromStdString("background-color:"+color));
        if(loadDisplay->saveBoxChecked()) //Data should be saved
        {
            recordCPU(outputFile, startLoad, loadDisplay->getFeedback());
        }
    }
    outputFile.close();
    loadDisplay->close();
    loadColor->close();
}
