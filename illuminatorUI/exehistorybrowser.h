#ifndef EXEHISTORYBROWSER_H
#define EXEHISTORYBROWSER_H

#include <QDialog>

namespace Ui {
class exeHistoryBrowser;
}

class exeHistoryBrowser : public QDialog
{
    Q_OBJECT

public:
    explicit exeHistoryBrowser(QWidget *parent = 0);
    ~exeHistoryBrowser();
    std::string filePath;
    std::vector<std::string> paths;
    void setFilePaths(std::vector<std::string> inputPaths);

private slots:
    void on_cancelButton_clicked();

    void on_chooseButton_clicked();

private:
    Ui::exeHistoryBrowser *ui;
};

#endif // EXEHISTORYBROWSER_H
