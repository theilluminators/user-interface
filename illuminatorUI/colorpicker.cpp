#include "colorpicker.h"
#include "ui_colorpicker.h"

colorPicker::colorPicker(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::colorPicker)
{
    ui->setupUi(this);
}

colorPicker::~colorPicker()
{
    delete ui;
}

void colorPicker::on_okButton_clicked()
{
    if(ui->blackButton->isChecked())
        chosenColor = "black";
    if(ui->blueButton->isChecked())
        chosenColor = "blue";
    if(ui->greenButton->isChecked())
        chosenColor = "green";
    if(ui->redButton->isChecked())
        chosenColor = "red";
    if(ui->orangeButton->isChecked())
        chosenColor = "orange";
    if(ui->whiteButton->isChecked())
        chosenColor = "white";
    if(ui->purpleButton->isChecked())
        chosenColor = "purple";
    if(ui->yellowButton->isChecked())
        chosenColor = "yellow";

    this->close();
}
